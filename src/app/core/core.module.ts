import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HighlightTextPipe } from '@core/pipe/highlight-text.pipe';

@NgModule({
  declarations: [HighlightTextPipe],
  exports: [
    HighlightTextPipe
  ],
  imports: [
    CommonModule
  ]
})
export class CoreModule { }
