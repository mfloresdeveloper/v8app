import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'v8-search-content',
  templateUrl: './search-content.component.html',
  styleUrls: ['./search-content.component.scss']
})
export class SearchContentComponent implements OnInit {

  @Input() items = [];
  @Input() pagination = {
    totals: 0,
    size: 5,
    page: 0
  };
  @Input() searchText = '';
  @Output() pageEvent = new EventEmitter();
  @Output() sizeEvent = new EventEmitter();
  public sizes = [5, 10, 15, 20];
  constructor() { }

  ngOnInit(): void {
  }

  pageChanged(value): void {
    this.pageEvent.emit(value);
  }

  changeSize(value): void {
    this.sizeEvent.emit(value);
  }
}
