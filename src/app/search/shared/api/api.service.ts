import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@env/environment';
import { ItemResponse } from '@search/shared/model/item';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private readonly URL = `${environment.api_resource}/v1` ;

  constructor(private httpClient: HttpClient) { }

  public getItems(params: any = {}): Observable<ItemResponse> {
    return this.httpClient.get<ItemResponse>(`${this.URL}/items`, { params });
  }
}
