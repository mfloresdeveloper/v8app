export class Item {
  categoryId: number;
  cost: number;
  description: string;
  name: string;
  price: number;
  quantity: number;
}

export class ItemResponse {
  content: Item[];
  number: number;
  numberOfElements: number;
  size: number;
  totalElements: number;
  totalPages: number;
}
