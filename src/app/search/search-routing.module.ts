import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContentComponent } from '@search/content/content.component';

const routes: Routes = [
  {
    path: 'search',
    component: ContentComponent,
  },
  {
    path: '',
    component: ContentComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SearchRoutingModule { }
