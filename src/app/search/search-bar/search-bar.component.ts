import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'v8-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {

  @Output() searchEvent = new EventEmitter();
  public searchForm: FormGroup;
  public submitted: boolean;

  constructor() { }

  ngOnInit(): void {
    this.searchForm = this.createFormGroup();
  }

  public createFormGroup(): FormGroup {
    return new FormGroup({
      search: new FormControl(null, [Validators.required]),
    });
  }

  onSubmit(): void {
    this.submitted = true;
    this.searchEvent.emit(this.searchForm.value.search);
  }

}
