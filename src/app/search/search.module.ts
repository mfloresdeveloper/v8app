import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchRoutingModule } from '@search/search-routing.module';
import { SharedModule } from '@shared/shared.module';
import { CoreModule } from '@core/core.module';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { SearchContentComponent } from './search-content/search-content.component';
import { ContentComponent } from './content/content.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginationModule } from 'ngx-bootstrap/pagination';

@NgModule({
  declarations: [SearchBarComponent, SearchContentComponent, ContentComponent],
  imports: [
    CommonModule,
    SearchRoutingModule,
    SharedModule,
    CoreModule,
    ReactiveFormsModule,
    PaginationModule,
    FormsModule
  ]
})
export class SearchModule { }
