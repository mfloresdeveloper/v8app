import { Component, OnInit } from '@angular/core';
import { ApiService } from '@search/shared/api/api.service';

@Component({
  selector: 'v8-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {

  public items = [];
  public pagination = {
    totals: 0,
    size: 5,
    page: 0
  };

  public params = {
    size: this.pagination.size,
    page: this.pagination.page,
    query: ''
  };

  public text = '';

  constructor(private api: ApiService) { }

  ngOnInit(): void {
  }

  searchEvent(search): void {
    this.text = search;
    this.items = [];
    this.pagination.totals = 0;
    if (search?.trim()) {
      const params =  {...this.params};
      params.query = search;
      this.fetchData(params);
    }
  }

  doFilterPage(value): void {
    const params =  {...this.params};
    params.page = value.page - 1;
    params.query = this.text;
    this.fetchData(params);
  }

  doChangeSize(value): void {
    this.params.size = value;
    const params =  {...this.params};
    params.page = 0;
    params.size = value;
    params.query = this.text;
    this.fetchData(params);
  }

  private fetchData(params: { size: number; query: string; page: number }): void {
    this.api.getItems(params).subscribe(value => {
      this.items = value.content;
      this.pagination.totals = value.totalElements;
      this.pagination.page = value.number;
      this.pagination.size = value.size;
    });
  }
}
