import { Component } from '@angular/core';

@Component({
  selector: 'v8-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'v8app';
}
